package com.example.homework_two;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button buttonHi = (Button) findViewById(R.id.button1);
		buttonHi.setOnClickListener(new View.OnClickListener()  {
			public void onClick(View v){
				//action
				TextView text = (TextView) findViewById(R.id.textView1);
				text.setText("Hello Matt Pendrick!");
			}
		});
		Button buttonBye = (Button) findViewById(R.id.button2);
		buttonBye.setOnClickListener(new View.OnClickListener()  {
			public void onClick(View v){
				//action
				TextView text = (TextView) findViewById(R.id.textView1);
				text.setText("Goodbye Matt Pendrick!");
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
}
